import itertools
# ------------------------------------------------------------------------------
# Festwiderstände aus E12 Reihe - https://de.wikipedia.org/wiki/E-Reihe
rCons = [1.0*10**3,  # from 1 kOhm
         1.2*10**3,
         1.5*10**3,
         1.8*10**3,
         2.2*10**3,
         2.7*10**3,
         3.3*10**3,
         3.9*10**3,
         4.7*10**3,
         5.6*10**3,
         6.8*10**3,
         8.2*10**3]  # to 8.2 kOhm
# from 1 kOhm to 82 MOhm
rCons = rCons +\
        [i * 10 for i in rCons] +\
        [i * 100 for i in rCons] +\
        [i * 1000 for i in rCons] +\
        [i * 10000 for i in rCons]

# ------------------------------------------------------------------------------
# maximaler Potentiometer-Widerstand
rPoti = [1.0*10**3,
         2.5*10**3,
         5.0*10**3]
# from 1 kOhm to 5 MOhm
rPoti = rPoti +\
        [i * 10 for i in rPoti] +\
        [i * 100 for i in rPoti] +\
        [i * 1000 for i in rPoti]

# ------------------------------------------------------------------------------
# Keramikkondensator aus E6 Reihe
cCera = [1.0*10**-9,  # from 1 nF
         1.5*10**-9,
         2.2*10**-9,
         3.3*10**-9,
         4.7*10**-9,
         6.8*10**-9]  # to 6.8 nF
# 1 nF bis 1 µF
cCera = cCera +\
        [i * 10 for i in cCera] +\
        [i * 100 for i in cCera] +\
        [i * 1000 for i in cCera]


r1 = 10 * 10**3  # kOhm
r2 = 100 * 10**3  # kOhm
c = 100 * 10**(-9)  # nF
t1 = 0.69*(r1+r2)*c
t2 = 0.69*(r2)*c

#      r1     r21    r22    c
pList=[rCons, rCons, rPoti, cCera]
pList=list(itertools.product(*pList))

fDeltaBest = 0
for permut in pList:
    r1 = permut[0]
    r21 = permut[1]
    r22 = permut[2]
    c = permut[3]

    # min f
    t1Min = 0.69*(r1+r21+r22)*c
    t2Min = 0.69*(r21+r22)*c
    # minimales Verhältnis zwischen Taktzeiten
    tQuoDifMin = abs(1-t1Min/t2Min)
    fMin = 1/(t1Min+t2Min)

    # max f
    t1Max = 0.69*(r1+r21)*c
    t2Max = 0.69*(r21)*c
    # maximales Verhältnis zwischen Taktzeiten
    tQuoDifMax = abs(1-t1Max/t2Max)
    fMax = 1/(t1Max+t2Max)

    fDelta = fMax-fMin

    # 16 Hz bis 20 kHz, ist fuer Menschen hoerbarer Schall
    # 200 Hz bis 800 Hz sinnvoll, zwei Oktaven
    if (fDelta > fDeltaBest and  # größter Tonumfang
        fMax < 800 and  # bis 800 Hz
        fMin > 200 and  # bis 200 Hz
        tQuoDifMin < 0.5 and  # max./min. Verhältnis zwischen Taktzeiten
        tQuoDifMax < 0.5):
            fDeltaBest = fDelta
            fMinBest = fMin
            fMaxBest = fMax
            r1Best = r1
            r21Best = r21
            r22Best = r22
            cBest = c

print("r1Best", r1Best)
print("r21Best", r21Best)
print("r22Best", r22Best)
print("cBest", cBest)
print("fMinBest", fMinBest)
print("fMaxBest", fMaxBest)
print("fDeltaBest", fDeltaBest)

r1 = r1Best
r2 = r21Best # + r22Best
c = cBest
t1 = 0.69*(r1+r2)*c
t2 = 0.69*(r2)*c
f = 1/(t1+t2)
print("t_1 =", t1)
print("t_2 =", t2)
print("T =", t1+t2)
