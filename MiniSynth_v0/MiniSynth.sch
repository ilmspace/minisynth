EESchema Schematic File Version 4
LIBS:MiniSynth-cache
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "MiniSynth"
Date "2019-10-28"
Rev "V0"
Comp "Space"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+9V #PWR03
U 1 1 5DB749B2
P 2700 1550
F 0 "#PWR03" H 2700 1400 50  0001 C CNN
F 1 "+9V" H 2715 1723 50  0000 C CNN
F 2 "" H 2700 1550 50  0001 C CNN
F 3 "" H 2700 1550 50  0001 C CNN
	1    2700 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5DB749E6
P 2700 3300
F 0 "#PWR04" H 2700 3050 50  0001 C CNN
F 1 "GND" H 2705 3127 50  0000 C CNN
F 2 "" H 2700 3300 50  0001 C CNN
F 3 "" H 2700 3300 50  0001 C CNN
	1    2700 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5DB74C29
P 3800 3150
F 0 "R1" V 4007 3150 50  0000 C CNN
F 1 "10 kOhm" V 3916 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3730 3150 50  0001 C CNN
F 3 "~" H 3800 3150 50  0001 C CNN
	1    3800 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2.1
U 1 1 5DB74C8F
P 4400 3150
F 0 "R2.1" V 4193 3150 50  0000 C CNN
F 1 "1 kOhm" V 4284 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4330 3150 50  0001 C CNN
F 3 "~" H 4400 3150 50  0001 C CNN
	1    4400 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5DB751B2
P 4400 3600
F 0 "RV1" V 4200 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 4300 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 4400 3600 50  0001 C CNN
F 3 "~" H 4400 3600 50  0001 C CNN
	1    4400 3600
	1    0    0    -1  
$EndComp
$Comp
L pspice:C C1
U 1 1 5DB753F5
P 1950 2500
F 0 "C1" V 2265 2500 50  0000 C CNN
F 1 "10 nF" V 2174 2500 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 1950 2500 50  0001 C CNN
F 3 "~" H 1950 2500 50  0001 C CNN
	1    1950 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 2700 3500 2700
Wire Wire Line
	3500 3150 3650 3150
$Comp
L pspice:C C2
U 1 1 5DB75736
P 3100 3150
F 0 "C2" V 3350 3250 50  0000 C CNN
F 1 "100 nF" V 3350 3000 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3100 3150 50  0001 C CNN
F 3 "~" H 3100 3150 50  0001 C CNN
	1    3100 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 2900 2700 3150
Wire Wire Line
	3350 3150 3500 3150
Connection ~ 3500 3150
Wire Wire Line
	2850 3150 2700 3150
Connection ~ 2700 3150
Wire Wire Line
	2700 3150 2700 3300
Wire Wire Line
	3500 3600 3500 3150
Wire Wire Line
	1450 2100 2200 2100
Wire Wire Line
	2200 2100 2200 2300
Wire Wire Line
	1450 2100 1450 3600
$Comp
L power:GND #PWR05
U 1 1 5DC0425E
P 4550 1800
F 0 "#PWR05" H 4550 1550 50  0001 C CNN
F 1 "GND" V 4650 1750 50  0000 C CNN
F 2 "" H 4550 1800 50  0001 C CNN
F 3 "" H 4550 1800 50  0001 C CNN
	1    4550 1800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DC047D1
P 1700 2500
F 0 "#PWR01" H 1700 2250 50  0001 C CNN
F 1 "GND" V 1600 2450 50  0000 C CNN
F 2 "" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	0    1    1    0   
$EndComp
$Comp
L power:+9V #PWR02
U 1 1 5DC0527D
P 2150 2700
F 0 "#PWR02" H 2150 2550 50  0001 C CNN
F 1 "+9V" V 2050 2750 50  0000 C CNN
F 2 "" H 2150 2700 50  0001 C CNN
F 3 "" H 2150 2700 50  0001 C CNN
	1    2150 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2150 2700 2200 2700
NoConn ~ 4400 3450
Wire Wire Line
	1450 3600 3500 3600
Wire Wire Line
	3500 2700 3500 3150
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5DC1F844
P 5300 2550
F 0 "J1" H 5380 2542 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 5380 2451 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5300 2550 50  0001 C CNN
F 3 "~" H 5300 2550 50  0001 C CNN
	1    5300 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR06
U 1 1 5DC077F9
P 4400 4350
F 0 "#PWR06" H 4400 4200 50  0001 C CNN
F 1 "+9V" H 4250 4400 50  0000 C CNN
F 2 "" H 4400 4350 50  0001 C CNN
F 3 "" H 4400 4350 50  0001 C CNN
	1    4400 4350
	-1   0    0    1   
$EndComp
$Comp
L power:+9V #PWR08
U 1 1 5DC22279
P 5100 2650
F 0 "#PWR08" H 5100 2500 50  0001 C CNN
F 1 "+9V" V 5000 2700 50  0000 C CNN
F 2 "" H 5100 2650 50  0001 C CNN
F 3 "" H 5100 2650 50  0001 C CNN
	1    5100 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5DC22AF9
P 5100 2550
F 0 "#PWR07" H 5100 2300 50  0001 C CNN
F 1 "GND" V 5000 2500 50  0000 C CNN
F 2 "" H 5100 2550 50  0001 C CNN
F 3 "" H 5100 2550 50  0001 C CNN
	1    5100 2550
	0    1    1    0   
$EndComp
$Comp
L Device:Speaker LS1
U 1 1 5DB75EEF
P 3900 1600
F 0 "LS1" V 4200 1600 50  0000 R CNN
F 1 "Speaker" V 4100 1700 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3900 1400 50  0001 C CNN
F 3 "~" H 3890 1550 50  0001 C CNN
	1    3900 1600
	0    -1   -1   0   
$EndComp
$Comp
L Timer:NE555 U1
U 1 1 5DB748F6
P 2700 2500
F 0 "U1" H 2450 2900 50  0000 C CNN
F 1 "NE555" H 2900 2900 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2700 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 2700 2500 50  0001 C CNN
	1    2700 2500
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:PN2222A Q1
U 1 1 5DF7E400
P 4350 1900
F 0 "Q1" V 4600 1950 50  0000 L CNN
F 1 "PN2222A" V 4700 1700 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4550 1825 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/PN/PN2222A.pdf" H 4350 1900 50  0001 L CNN
	1    4350 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R RQ1
U 1 1 5DF7EF1B
P 3600 2300
F 0 "RQ1" V 3807 2300 50  0000 C CNN
F 1 "1 kOhm" V 3716 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 2300 50  0001 C CNN
F 3 "~" H 3600 2300 50  0001 C CNN
	1    3600 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_LS1
U 1 1 5DF88EC5
P 3600 1800
F 0 "R_LS1" V 3807 1800 50  0000 C CNN
F 1 "100 Ohm" V 3716 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 1800 50  0001 C CNN
F 3 "~" H 3600 1800 50  0001 C CNN
	1    3600 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 1800 3800 1800
Wire Wire Line
	4000 1800 4100 1800
$Comp
L Switch:SW_Push SW1
U 1 1 5DF8F417
P 4400 3950
F 0 "SW1" V 4450 4100 50  0000 L CNN
F 1 "SW" V 4350 4150 50  0000 L CNN
F 2 "" H 4400 4150 50  0001 C CNN
F 3 "~" H 4400 4150 50  0001 C CNN
	1    4400 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D1
U 1 1 5DF97D51
P 3950 2050
F 0 "D1" H 3900 2150 50  0000 L CNN
F 1 "D" H 3900 1950 50  0000 L CNN
F 2 "" H 3950 2050 50  0001 C CNN
F 3 "~" H 3950 2050 50  0001 C CNN
	1    3950 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2050 3800 1800
Connection ~ 3800 1800
Wire Wire Line
	3800 1800 3900 1800
Wire Wire Line
	4100 2050 4100 1800
Connection ~ 4100 1800
Wire Wire Line
	4100 1800 4150 1800
Wire Wire Line
	4550 3600 4550 3350
Wire Wire Line
	4550 3350 4550 3150
$Comp
L Device:R_POT RV2
U 1 1 5DF9F135
P 4850 3600
F 0 "RV2" V 4650 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 4750 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 4850 3600 50  0001 C CNN
F 3 "~" H 4850 3600 50  0001 C CNN
	1    4850 3600
	1    0    0    -1  
$EndComp
NoConn ~ 4850 3450
$Comp
L Switch:SW_Push SW2
U 1 1 5DF9F13C
P 4850 3950
F 0 "SW2" V 4900 4100 50  0000 L CNN
F 1 "SW" V 4800 4150 50  0000 L CNN
F 2 "" H 4850 4150 50  0001 C CNN
F 3 "~" H 4850 4150 50  0001 C CNN
	1    4850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 3350 5000 3350
Connection ~ 4550 3350
$Comp
L Device:R_POT RV3
U 1 1 5DFA56E2
P 5300 3600
F 0 "RV3" V 5100 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 5200 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 5300 3600 50  0001 C CNN
F 3 "~" H 5300 3600 50  0001 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
NoConn ~ 5300 3450
$Comp
L Switch:SW_Push SW3
U 1 1 5DFA56E9
P 5300 3950
F 0 "SW3" V 5350 4100 50  0000 L CNN
F 1 "SW" V 5250 4150 50  0000 L CNN
F 2 "" H 5300 4150 50  0001 C CNN
F 3 "~" H 5300 4150 50  0001 C CNN
	1    5300 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 3600 5450 3350
$Comp
L Device:R_POT RV4
U 1 1 5DFA56F1
P 5750 3600
F 0 "RV4" V 5550 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 5634 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 5750 3600 50  0001 C CNN
F 3 "~" H 5750 3600 50  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
NoConn ~ 5750 3450
$Comp
L Switch:SW_Push SW4
U 1 1 5DFA56F8
P 5750 3950
F 0 "SW4" V 5800 4100 50  0000 L CNN
F 1 "SW" V 5700 4150 50  0000 L CNN
F 2 "" H 5750 4150 50  0001 C CNN
F 3 "~" H 5750 4150 50  0001 C CNN
	1    5750 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 3600 5900 3350
$Comp
L Device:R_POT RV5
U 1 1 5DFA7094
P 6200 3600
F 0 "RV5" V 6000 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 6084 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 6200 3600 50  0001 C CNN
F 3 "~" H 6200 3600 50  0001 C CNN
	1    6200 3600
	1    0    0    -1  
$EndComp
NoConn ~ 6200 3450
$Comp
L Switch:SW_Push SW5
U 1 1 5DFA709B
P 6200 3950
F 0 "SW5" V 6250 4100 50  0000 L CNN
F 1 "SW" V 6150 4150 50  0000 L CNN
F 2 "" H 6200 4150 50  0001 C CNN
F 3 "~" H 6200 4150 50  0001 C CNN
	1    6200 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_POT RV6
U 1 1 5DFA70A3
P 6650 3600
F 0 "RV6" V 6450 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 6534 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 6650 3600 50  0001 C CNN
F 3 "~" H 6650 3600 50  0001 C CNN
	1    6650 3600
	1    0    0    -1  
$EndComp
NoConn ~ 6650 3450
$Comp
L Switch:SW_Push SW6
U 1 1 5DFA70AA
P 6650 3950
F 0 "SW6" V 6700 4100 50  0000 L CNN
F 1 "SW" V 6600 4150 50  0000 L CNN
F 2 "" H 6650 4150 50  0001 C CNN
F 3 "~" H 6650 4150 50  0001 C CNN
	1    6650 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 3600 6800 3350
Wire Wire Line
	5450 3350 5900 3350
Connection ~ 5450 3350
Wire Wire Line
	5900 3350 6350 3350
Wire Wire Line
	6350 3350 6350 3600
Connection ~ 5900 3350
Wire Wire Line
	6350 3350 6800 3350
Connection ~ 6350 3350
Wire Wire Line
	5000 3600 5000 3350
Connection ~ 5000 3350
Wire Wire Line
	5000 3350 5450 3350
Wire Wire Line
	4400 4150 4850 4150
Wire Wire Line
	4850 4150 5300 4150
Connection ~ 4850 4150
Wire Wire Line
	5300 4150 5750 4150
Connection ~ 5300 4150
Wire Wire Line
	5750 4150 6200 4150
Connection ~ 5750 4150
Wire Wire Line
	6200 4150 6650 4150
Connection ~ 6200 4150
$Comp
L Device:R_POT RV7
U 1 1 5DFAE092
P 7100 3600
F 0 "RV7" V 6900 3600 50  0000 C CNN
F 1 "0-25 kOhm" V 6984 3600 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 7100 3600 50  0001 C CNN
F 3 "~" H 7100 3600 50  0001 C CNN
	1    7100 3600
	1    0    0    -1  
$EndComp
NoConn ~ 7100 3450
$Comp
L Switch:SW_Push SW7
U 1 1 5DFAE099
P 7100 3950
F 0 "SW7" V 7150 4100 50  0000 L CNN
F 1 "SW" V 7050 4150 50  0000 L CNN
F 2 "" H 7100 4150 50  0001 C CNN
F 3 "~" H 7100 4150 50  0001 C CNN
	1    7100 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 3600 7250 3350
Wire Wire Line
	6800 3350 7250 3350
Connection ~ 6800 3350
Wire Wire Line
	7100 4150 6650 4150
Connection ~ 6650 4150
Wire Wire Line
	4400 4150 4400 4350
Connection ~ 4400 4150
Wire Wire Line
	3950 3150 4100 3150
Wire Wire Line
	3200 2500 4100 2500
Wire Wire Line
	4100 2500 4100 3150
Connection ~ 4100 3150
Wire Wire Line
	4100 3150 4250 3150
Wire Wire Line
	2700 1550 2700 1800
Wire Wire Line
	3450 1800 2700 1800
Connection ~ 2700 1800
Wire Wire Line
	2700 1800 2700 2100
Wire Wire Line
	3450 2300 3200 2300
Wire Wire Line
	3750 2300 4350 2300
Wire Wire Line
	4350 2100 4350 2300
$EndSCHEMATC
